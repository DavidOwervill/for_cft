from fastapi import FastAPI, Depends, HTTPException
from sqlalchemy.orm import Session
from auth.auth_handler import sign_jwt, get_current_user
from auth.auth_bearer import JWTBearer
from auth.hashing import Hasher
from sql_app import crud, models, schemas
from sql_app.database import SessionLocal, engine


models.Base.metadata.create_all(bind=engine)

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_current_user_jwt(token: str = Depends(JWTBearer())):
    user = get_current_user(token)
    if not user:
        raise HTTPException(status_code=400, detail="User not found")
    return user


@app.post("/login/")
async def user_login(user: schemas.UserCreate, db: Session = Depends(get_db)):
    user_login = crud.get_user_by_username(db=db, username=user.username)
    if user_login is not None:
        if user.username == user_login.username and Hasher.verify_password(user.password, user_login.hashed_password):
            return sign_jwt(user.username)
    else:
        raise HTTPException(status_code=400, detail="User not found")


@app.get("/payments/", dependencies=[Depends(JWTBearer())], response_model=schemas.User)
def main(db: Session = Depends(get_db), current_user=Depends(get_current_user_jwt)):
    user = db.query(models.User).filter(models.User.username == current_user).first()
    users = crud.get_payments(db, username=user.username)
    return users
