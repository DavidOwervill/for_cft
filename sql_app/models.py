from sqlalchemy import Float, Column, Integer, String
from .database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String)
    hashed_password = Column(String)
    email = Column(String)
    payment = Column(Float)
    date_next_upgrade = Column(String)
