from pydantic import BaseModel


class UserBase(BaseModel):
    username: str


class UserCreate(UserBase):
    password: str


class User(BaseModel):
    username: str
    payment: float | None
    date_next_upgrade: str | None

    class Config:
        orm_mode = True
